import React, {Component} from 'react';
import './css/App.css';

class Navigation extends Component {

  constructor(props) {
    super(props);
    this.onMenuClick = this.onMenuClick.bind(this);
  }

  onMenuClick(event) {
    this.props.onMenuClick(event.target.value);
  }

  render() {
    return (
      <div className="Navigation">
        <a href="/mevango">Mevango</a>
        <a href="/ifa16">IFA16</a>
        <a href="/womensmarch">Womensmarch</a>
      </div>
    );
  }
}

export default Navigation;
