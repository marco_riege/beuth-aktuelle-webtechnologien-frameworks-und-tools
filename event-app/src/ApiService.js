import $ from 'jquery';

class ApiService {

  static getSyncTweetsAjax(topic, filter = '', limit = 12, offset = 0) {
    var contentData = null
    let filterArg = '';

    if (filter !== '')
      filterArg = 'filter_service==' + filter + '&'

    $.ajax({
      url: "https://api.flimme.com/api/v1/events/" + topic + "/media?" + filterArg + "number=" + limit + "&offset=" + offset,
      async: false,
      dataType: 'json',
      success: function(data) {
        contentData = data;
      },
      error: function(data) {
        contentData = [];
      }
    });
    return contentData;
  }

  static getSyncEventAjax(topic) {
    var contentData = null

    $.ajax({
      url: "https://api.flimme.com/api/v1/events/" + topic,
      async: false,
      dataType: 'json',
      success: function(data) {
        contentData = data;
      },
      error: function(data) {
        contentData = [];
      }
    });
    return contentData;
  }
}
export default ApiService;
