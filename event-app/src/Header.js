import React, {Component} from 'react';
import Navigation from './Navigation.js';
import Information from './Information.js';
import Filter from './Filter.js';
import ApiService from './ApiService.js';
import notfound from './img/notfound.jpg';
import './css/header.css';

class Header extends Component {

  constructor(props) {
    super(props);
    this.onClickInformation = this.onClickInformation.bind(this);
    this.onClickFilter = this.onClickFilter.bind(this);
    this.elements = 180;
    this.selectedTopic = 'mevango'
  }

  componentWillMount() {
    this.state = {
      showComponentInformation: false,
      showComponentFilter: false,
      imagegrid: ""
    };
    this.loadContentSynch(this.props.contentType, this.elements, 0);
  }

  onClickInformation() {
    if (this.state.showComponentFilter) {
      this.setState({showComponentFilter: false});
    }
    if (this.state.showComponentInformation) {
      this.setState({showComponentInformation: false});
    } else {
      this.setState({showComponentInformation: true});
    }
  }

  onClickFilter() {
    if (this.state.showComponentInformation) {
      this.setState({showComponentInformation: false});
    }
    if (this.state.showComponentFilter) {
      this.setState({showComponentFilter: false});
    } else {
      this.setState({showComponentFilter: true});
    }
  }
  onError(index) {
    this.src = notfound;
  }

  loadContentSynch(topic, numberOfElements, offset) {
    let data = ApiService.getSyncTweetsAjax(topic, '', numberOfElements, offset);
    if (data.length === 0) {
      this.setState({hasMore: false});
    }
    this.setState({
      content: data
    }, () => {});
  }

  render() {
    var images = [],
      i = 0,
      len = this.state.content.length;
    while (++i < len) 
      images.push(this.state.content[i].image + ":thumb");

    return (
      <div>
        <div className="Header">
          <div className="HeaderBackground">
            <ul className="backgroundImageList">
              {images.map((name, index) => {
                return <li key={index}><img key={index} src={name} onError={(event) => {
                  event.target.setAttribute("src", notfound);
                }} className="backgroundImage" alt=""/></li>;
              })}
            </ul>
          </div>

          <Navigation className="Navigation" onMenuClick={this.props.onMenuClick}></Navigation>
          <p className="EventName">
            {this.props.contentType}
          </p>
          <div className="subNavigation">
            <button onClick={this.onClickInformation}>
              Information
            </button>
            <button onClick={this.onClickFilter}>
              Filter
            </button>
          </div>

        </div>
        {this.state.showComponentInformation
          ? <Information contentType={this.props.contentType}/>
          : null}

        {this.state.showComponentFilter
          ? <Filter onFilterClick={this.props.onFilterClick}/>
          : null}
      </div>
    );
  }
}

export default Header;
