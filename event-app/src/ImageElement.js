import React, {Component} from 'react';
import notfound from './img/notfound.jpg';
import './css/bootstrap.css';
import './css/imagegrid.css';
import './css/tweetelement.css';

class ImageElement extends Component {

  componentWillMount() {
    this.state = {
      showComponent: false
    };
  }

  render() {
    let coverImg = this.props.tweet.image
      ? this.props.tweet.image + ":medium"
      : notfound
    let accountName = this.props.tweet
      ? this.props.tweet.owner.username
      : "@AccountName"
    let name = this.props.tweet
      ? this.props.tweet.owner.name
      : "User_Name"

    let profileIcon = this.props.tweet.owner.picture
      ? this.props.tweet.owner.picture
      : notfound

    let social_icon = "social_icon icon emt-" + this.props.tweet.service;

    return (
      <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div className="image__cell is-collapsed flex_item" onClick={this.props.onClickEvent}>
          <div className="image--basic">
            <img src={coverImg} alt="" onError={(event) => {
              event.target.setAttribute("src", notfound);
            }} className="flex_img img-responsive"/>
            <img src={profileIcon} alt="" onError={(event) => {
              event.target.setAttribute("src", notfound);
            }} className="profile"/>
            <p className="user_name">
              {accountName}
            </p>
            <p className="user_fullname">
              {name}
            </p>
            <span className={social_icon}></span>

          </div>
        </div>
      </div>
    );
  }
}

export default ImageElement;
