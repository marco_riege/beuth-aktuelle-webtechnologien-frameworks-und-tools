import React, {Component} from 'react';
import './css/filter.css';

class Filter extends Component {

  onFilterClick(event, val = '') {
    this.props.onFilterClick(val);
  }

  render() {
    return (
      <div className="body">
        <p className="filter">
          <span className="icon emt-facebook" onClick={(e) => this.onFilterClick(e, "twitter,instagram,google")} value="facebook"></span>
          <span className="icon emt-twitter" onClick={(e) => this.onFilterClick(e, "facebook,instagram,google")} value="twitter"></span>
          <span className="icon emt-instagram" onClick={(e) => this.onFilterClick(e, "facebook,twitter,google")} value="instagram"></span>
          <span className="icon emt-google-plus" onClick={(e) => this.onFilterClick(e, "facebook,twitter,instagram")} value="google"></span>
        </p>
      </div>
    );
  }
}

export default Filter;
