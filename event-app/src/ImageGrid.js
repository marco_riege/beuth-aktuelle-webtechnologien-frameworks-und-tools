import React, {Component} from 'react';
import ReactModal from 'react-modal';
import InfiniteScroll from 'react-infinite-scroller';
import ImageElement from './ImageElement.js';
import ApiService from './ApiService.js';
import notfound from './img/notfound.jpg';
import './css/bootstrap.css'
import './css/detailview.css'
import './css/imagegrid.css';

class ImageGrid extends Component {

  constructor(props) {
    super(props);
    this.loadMore = this.loadMore.bind(this);
    this.createTweetElements = this.createTweetElements.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.elements = 12;
  }

  componentWillMount() {
    this.state = {
      hasMore: true,
      items: [],
      showComponent: false,
      content: '',
      detailView: ''
    };

    //this.loadContentAsynch("mevango",12,0);
    this.loadContentSynch(this.props.contentType, this.props.filter, this.elements, 0);
  }

  handleOpenModal(id) {
    var clickedItem = this.state.items.find(function(obj) {
      return obj.key === id;
    });

    if (clickedItem) {
      let img = clickedItem.props.tweet.image
        ? clickedItem.props.tweet.image
        : notfound;
      let profile_img = clickedItem.props.tweet.owner.picture
        ? clickedItem.props.tweet.owner.picture
        : notfound;

      let detailView = (
        <div >
          <div className="expand_img_container"><img className="expand_img" src={img} alt="" onError={(event) => {
            event.target.setAttribute("src", notfound);
          }}/>
          </div>
          <img className="expand_profile" src={profile_img} alt="" onError={(event) => {
            event.target.setAttribute("src", notfound);  }} />
          <p className="expand_username">
            {clickedItem.props.tweet.owner.name}
          </p>
          <p className="expand_fullname">
            {clickedItem.props.tweet.owner.username}</p>
          <p className="expand_post">
            {< td dangerouslySetInnerHTML = {{__html: clickedItem.props.tweet.description}}/>}
          </p>
          <button className="expand_close" onClick={this.handleCloseModal}></button>
        </div>
      );
      this.setState({detailView: detailView, showModal: true});
    }
  }

  handleCloseModal() {
    this.setState({showModal: false});
  }

  loadMore(page) {
    this.loadContentSynch(this.props.contentType, this.props.filter, this.elements, page * this.elements);
    this.setState({
      items: this.state.items.concat(this.createTweetElements(page)),
      hasMore: (this.state.hasMore)
    });
  }


  loadContentSynch(topic, filter, numberOfElements, offset) {
    let data = ApiService.getSyncTweetsAjax(topic, filter, numberOfElements, offset);
    if (data.length === 0) {
      this.setState({hasMore: false});
    }
    this.setState({
      content: data
    });
  }

  createTweetElements(page) {
    var rows = [];
    for (var i = 0; i < this.state.content.length; i++) {
      rows.push(<ImageElement key={page + "_" + i} tweet={this.state.content[i]} onClickEvent={this.handleOpenModal.bind(this, page + "_" + i)}/>);
    }
    return rows;
  }

  render() {
    return (
      <div>
        <InfiniteScroll pageStart={0} loadMore={this.loadMore} hasMore={this.state.hasMore} loader={< div className = "loader" > Loading ...</div>}>
          <ReactModal className="expand_item" isOpen={this.state.showModal} onRequestClose={this.handleCloseModal} contentLabel="Modal">
            {this.state.detailView}
          </ReactModal>
          <div className="container-fluid">
            <div className="row">
              {this.state.items}
            </div>
          </div>
        </InfiniteScroll>
      </div>
    );
  }
}

export default ImageGrid;
