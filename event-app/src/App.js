import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import AppContainer from './AppContainer.js';
import './css/App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route path="/:name?" component={AppContainer}/>
        </div>
      </Router>
    );
  }
}

export default App;
