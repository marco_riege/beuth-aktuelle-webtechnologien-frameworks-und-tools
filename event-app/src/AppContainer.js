import React, {Component} from 'react';
import Header from './Header.js';
import ImageGrid from './ImageGrid.js'
import './css/header.css';

class AppContainer extends Component {

  constructor(props) {
    super(props);
    this.selectedTopic = props.match.params.name //because state is asynchron
    if(this.selectedTopic === undefined) {
      this.selectedTopic = "mevango"
    }

    this.filter = ''
    this.state = {
      refresh: false
    };
    this.refreshGrid = false;
  }

  onMenuClick(topic) {
    this.selectedTopic = topic;
    this.filter = '';
    this.setState({refresh: true}); // update html-render
  }

  onFilterClick(filter) {
    this.filter = filter;
    this.setState({refresh: true}); // update html-render
  }

  setRefreshsBack() {
    this.refreshGrid = false;
  }

  render() {
    return (
      <div>
        <Header key={'headerID_' + this.selectedTopic} contentType={this.selectedTopic} onFilterClick={this.onFilterClick.bind(this)} onMenuClick={this.onMenuClick.bind(this)}/>
        <ImageGrid key={'imageGridID_' + this.selectedTopic + '_filter' + this.filter} filter={this.filter} contentType={this.selectedTopic}/>
      </div>
    );
  }
}

export default AppContainer;
