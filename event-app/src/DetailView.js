import React, {Component} from 'react';
import logo from './img/notfound.jpg';
import profile from './img/notfound.jpg';
import './css/tweetelement.css';

class DetailView extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="item">
          <img src={logo} className="img"/>
          <img src={profile} className="profile_img"/>
          <p className="user_name">
            @AccountName
          </p>
          <p className="user_fullname">
            FirstName Surename
          </p>
        </div>
      </div>
    );
  }
}

export default DetailView;
