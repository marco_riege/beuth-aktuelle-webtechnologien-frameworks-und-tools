import React, {Component} from 'react';
import ApiService from './ApiService.js';
import './css/information.css';

class Information extends Component {
  componentWillMount() {
    this.loadDescriptionSynch(this.props.contentType);
  }

  loadDescriptionSynch(topic) {
    let data = ApiService.getSyncEventAjax(topic);
    if (data.length === 0) {
      this.setState({hasMore: false});
    }
    this.setState({
      eventData: data
    }, () => {});
  }

  render() {
    return (
      <div className="informationText">
        <p className="text">{this.state.eventData.description}
        </p>
      </div>
    );
  }
}

export default Information;
